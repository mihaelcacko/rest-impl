package rznu.lab1.twitterlite.controllers;

import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;
import rznu.lab1.twitterlite.models.User;
import rznu.lab1.twitterlite.models.UserModel;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class UserRepresentationModelAssembler implements RepresentationModelAssembler<User, UserModel> {
    @Override
    public UserModel toModel(User user) {
        return new UserModel(user).add(
                linkTo(methodOn(UserController.class).getUser(user.getId())).withSelfRel(),
                linkTo(methodOn(UserController.class).getUserList()).withRel("users"),
                linkTo(methodOn(UserController.class).getPosts(user.getId())).withRel("posts"));
    }
}
