package rznu.lab1.twitterlite.controllers;

public class PostNotFoundException extends RuntimeException {
    public PostNotFoundException(Long id) {
        super("Could not find post" + id);
    }
}
