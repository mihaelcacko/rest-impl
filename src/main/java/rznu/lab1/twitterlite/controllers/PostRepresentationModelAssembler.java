package rznu.lab1.twitterlite.controllers;

import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;
import rznu.lab1.twitterlite.models.Post;
import rznu.lab1.twitterlite.models.PostModel;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class PostRepresentationModelAssembler implements RepresentationModelAssembler<Post, PostModel> {

    @Override
    public PostModel toModel(Post post) {
        return new PostModel(post).add(
                linkTo(methodOn(PostController.class).getPost(post.getId())).withSelfRel(),
                linkTo(methodOn(UserController.class).getUser(post.getUser().getId())).withRel("user"),
                linkTo(methodOn(PostController.class).getPostList()).withRel("posts"));
    }

}
