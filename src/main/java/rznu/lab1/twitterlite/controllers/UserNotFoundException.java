package rznu.lab1.twitterlite.controllers;

public class UserNotFoundException extends RuntimeException {

    public UserNotFoundException(Long id) {
        super("Could not find user" + id);
    }
}
