package rznu.lab1.twitterlite.controllers;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rznu.lab1.twitterlite.models.Post;
import rznu.lab1.twitterlite.models.PostModel;
import rznu.lab1.twitterlite.models.User;
import rznu.lab1.twitterlite.repositories.PostRepository;
import rznu.lab1.twitterlite.repositories.UserRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping("/api/posts")
public class PostController {
    public final PostRepository repository;
    public final UserRepository userRepository;
    public final PostRepresentationModelAssembler assembler;

    public PostController(PostRepository repository, PostRepresentationModelAssembler assembler, UserRepository userRepository){
        this.assembler = assembler;
        this.repository = repository;
        this.userRepository = userRepository;
    }

    @GetMapping()
    public CollectionModel<PostModel> getPostList() {
        List<PostModel> posts = repository.findAll().stream()
                .map(assembler::toModel)
                .collect(Collectors.toList());
        return new CollectionModel<>(posts,
                linkTo(methodOn(this.getClass()).getPostList()).withSelfRel());
    }

    @PostMapping()
    public ResponseEntity<?> newPost(@RequestBody Post newPost){
        if(newPost.getUser() == null){
            return ResponseEntity.badRequest().build();
        }
        repository.save(newPost);
        User user = userRepository.findById(newPost.getUser().getId())
                .orElseThrow(()-> new UserNotFoundException(newPost.getUser().getId()));
        user.getPosts().add(newPost);
        userRepository.save(user);
        return ResponseEntity
                .created(linkTo(methodOn(this.getClass()).getPost(newPost.getId())).toUri())
                .build();
    }

    @GetMapping("/{id}")
    public PostModel getPost(@PathVariable Long id){
        Post post = repository.findById(id)
                .orElseThrow(() -> new PostNotFoundException(id)); //fix
        return assembler.toModel(post);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updatePost(@RequestBody Post newPost, @PathVariable Long id){
        if(repository.existsById(id)){
            repository.findById(id)
                    .map(post -> {
                        post.setBody(newPost.getBody());
                        post.setTitle(newPost.getTitle());
                        return repository.save(post);
                    });
            return ResponseEntity.ok().build();
        }else{
            newPost.setId(id);
            repository.save(newPost);
            return ResponseEntity.created(linkTo(methodOn(this.getClass()).getPost(newPost.getId())).toUri())
                    .build();
        }
    }

    @DeleteMapping("/{id}")
    public void deletePost(@PathVariable Long id) {
        if(repository.existsById(id)){
            Post post = repository.findById(id).get();
            post.getUser().getPosts().remove(post);
            repository.deleteById(id);
        }else{
            throw new PostNotFoundException(id);
        }

    }

}
