package rznu.lab1.twitterlite.controllers;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rznu.lab1.twitterlite.models.Post;
import rznu.lab1.twitterlite.models.PostModel;
import rznu.lab1.twitterlite.models.User;
import rznu.lab1.twitterlite.models.UserModel;
import rznu.lab1.twitterlite.repositories.PostRepository;
import rznu.lab1.twitterlite.repositories.UserRepository;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/users")
public class UserController {
    private final UserRepository repository;
    private final PostRepository postRepository;
    private final UserRepresentationModelAssembler assembler;
    private final PostRepresentationModelAssembler postAssembler;

    public UserController(UserRepository repository, UserRepresentationModelAssembler assembler,
                          PostRepresentationModelAssembler postAssembler, PostRepository postRepository){
        this.repository = repository;
        this.assembler = assembler;
        this.postAssembler = postAssembler;
        this.postRepository = postRepository;
    }

    @GetMapping()
    public CollectionModel<UserModel> getUserList() {
       List<UserModel> users = repository.findAll().stream()
               .map(assembler::toModel)
               .collect(Collectors.toList());
       return new CollectionModel<>(users,
               linkTo(methodOn(UserController.class).getUserList()).withSelfRel());
    }

    @PostMapping()
    public ResponseEntity<?> newUser(@RequestBody User newUser){
        repository.save(newUser);
        return ResponseEntity
                .created(linkTo(methodOn(this.getClass()).getUser(newUser.getId())).toUri())
                .build();
    }

    @GetMapping("/{id}")
    public UserModel getUser(@PathVariable Long id){
        User user = repository.findById(id)
                .orElseThrow(() -> new UserNotFoundException(id));
        return assembler.toModel(user);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateUser(@RequestBody User newUser, @PathVariable Long id){
        if(repository.existsById(id)){
            repository.findById(id)
                    .map(user -> {
                        user.setUsername(newUser.getUsername());
                        user.setEmail(newUser.getEmail());
                        return repository.save(user);
                    });
            return ResponseEntity.ok().build();
        }else {
            newUser.setId(id);
            repository.save(newUser);
            return  ResponseEntity.created(linkTo(methodOn(this.getClass()).getUser(newUser.getId())).toUri())
                    .build();
        }
    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable Long id) {
        for (Post temp: repository.findById(id).get().getPosts()) {
            postRepository.deleteById(temp.getId());
        }
        repository.deleteById(id);
    }

    @GetMapping("/{id}/posts")
    public ResponseEntity<?> getPosts(@PathVariable Long id){
        if(!repository.existsById(id)){
            return ResponseEntity.notFound().build();
        }
        List<PostModel> posts = repository.findById(id).get().getPosts()
                .stream().map(postAssembler::toModel).collect(Collectors.toList());
        return ResponseEntity.ok(new CollectionModel<>(posts,
               linkTo(methodOn(UserController.class).getPosts(id)).withSelfRel()));
    }
}
