package rznu.lab1.twitterlite.models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Post {

    private @Id @GeneratedValue Long id;
    @ManyToOne
    private User user;

    private String title;
    private String body;

    public Post() {}

    public Post(String title,String body, User user){
        this.title=title;
        this.body=body;
        this.user=user;
    }
}
