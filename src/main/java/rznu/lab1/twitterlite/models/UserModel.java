package rznu.lab1.twitterlite.models;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

@Data
@EqualsAndHashCode(callSuper=false)
@Relation(collectionRelation = "users")
public class UserModel extends RepresentationModel<UserModel> {
    private String username;
    private String email;

    public UserModel(){}

    public UserModel(User user){
        this.username = user.getUsername();
        this.email = user.getEmail();
    }

}
