package rznu.lab1.twitterlite.models;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.LinkedList;
import java.util.List;

@Data
@Entity
public class User {

	private @Id @GeneratedValue Long id;
	@OneToMany(mappedBy = "user")
	private List<Post> posts;
	private String username;
	private String password;
	private String email;
	
	public User() {}
	
	public User(String name, String email, String password){
		this.username = name;
		this.email = email;
		this.password= password;
		posts = new LinkedList<>();
	}
}
