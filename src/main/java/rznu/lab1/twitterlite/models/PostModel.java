package rznu.lab1.twitterlite.models;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

@Data
@EqualsAndHashCode(callSuper=false)
@Relation(collectionRelation = "posts")
public class PostModel extends RepresentationModel<PostModel> {

    private String title;
    private String body;
    private String op;

    public PostModel(Post post){
        this.title=post.getTitle();
        this.body=post.getBody();
        this.op=post.getUser().getUsername();
    }
}
