package rznu.lab1.twitterlite;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;

import org.springframework.stereotype.Component;
import rznu.lab1.twitterlite.models.Post;
import rznu.lab1.twitterlite.repositories.PostRepository;
import rznu.lab1.twitterlite.repositories.UserRepository;
import rznu.lab1.twitterlite.models.User;

@Component
public class LoadDatabase implements CommandLineRunner {
	@Autowired UserRepository userRepo;
	@Autowired PostRepository postRepo;

	@Override
	public void run(String... args) throws Exception {
		User user1 = new User("user1","user1@gmail.com","password");
		userRepo.save(user1);
		User user2 = new User("user2","user2@outlook.com","password");
		userRepo.save(user2);
		Post post1 = new Post("Title", "Hello World", user1);
		postRepo.save(post1);
		user1.getPosts().add(post1);
		Post post2 = new Post("Title 2", "Hello Hell", user1);
		user1.getPosts().add(post2);
		postRepo.save(post2);
		Post post3 = new Post("Titlex", "Post body 1", user1);
		Post post4 = new Post("Titlez", "Hello ....", user2);
		user1.getPosts().add(post3);
		postRepo.save(post3);
		user2.getPosts().add(post4);
		postRepo.save(post4);
		userRepo.save(user1);
		userRepo.save(user2);
	}
}
