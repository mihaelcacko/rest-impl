package rznu.lab1.twitterlite.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import rznu.lab1.twitterlite.models.User;

public interface UserRepository extends JpaRepository<User, Long>{
    User findByUsername(String username);
}
