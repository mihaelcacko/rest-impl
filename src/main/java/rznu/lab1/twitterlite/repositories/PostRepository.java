package rznu.lab1.twitterlite.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import rznu.lab1.twitterlite.models.Post;

public interface PostRepository extends JpaRepository<Post, Long> {
}
