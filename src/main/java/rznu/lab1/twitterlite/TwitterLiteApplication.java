package rznu.lab1.twitterlite;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TwitterLiteApplication {

	public static void main(String[] args) {
		SpringApplication.run(TwitterLiteApplication.class, args);
	}

}
