package rznu.lab1.TwitterLite;

import io.restassured.RestAssured;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import rznu.lab1.twitterlite.models.Post;
import rznu.lab1.twitterlite.models.User;

import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class TwitterLiteApplicationTests {

	public static String USERNAME = "user1";
	public static String PASS = "password";

	@BeforeEach
	public  void setup() {
		RestAssured.port=5051;
	}
	@AfterEach
    public  void teardown(){
	    RestAssured.reset();
    }

	@Test
	public void whenRequestGetAndNotLoggedIn_thenUnAuthorized() {
		when().request("GET", "/api/users").then().statusCode(401);
	}

	@Test
	public void whenProvidedCredentials_thenLogIn(){
		given().
				param("username", "user2").
				param("password", "password").
				when().
				post("/api/login")
				.then().statusCode(302);
	}

	@Test
	public void whenRequestGetUsers_thenOk(){
		given().auth().preemptive()
				.basic("user2", "password")
				.when().get("/api/users")
				.then().statusCode(200);
	}

	@Test
	public void whenRequestPostUsers_thenCreated(){
		User newUser = new User("user3","hotmail","mypass");
		given().auth().preemptive()
                .basic("user2", "password")
				.contentType("application/json").body(newUser)
				.when().post("/api/users")
				.then().statusCode(201);
	}

	@Test
	public void whenRequestGetUserId_thenOk(){
		given().auth().preemptive()
                .basic("user2", "password")
				.when().get("/api/users/{id}", 1)
				.then().statusCode(200);
	}

	@Test
	public void whenRequestPutExistingUsers_thenOk(){
		User newUser = new User("User1","user1@outlook.com",PASS);
		given().auth().preemptive()
                .basic("user2", "password")
				.contentType("application/json").body(newUser)
				.when().put("/api/users/{id}",1)
				.then().statusCode(200);
	}

	@Test
	public void whenRequestPutNewUsers_thenCreated(){
		User newUser = new User("user4","user4@gmail.com", "123456");
		given().auth().preemptive()
				.basic(USERNAME, PASS)
				.contentType("application/json").body(newUser)
				.when().put("/api/users/{id}",10)
				.then().statusCode(201);
	}

	@Test
	public void whenRequestDeleteUserId_thenOk(){
		given().auth().preemptive()
				.basic(USERNAME, PASS)
				.when().delete("/api/users/{id}", 2)
				.then().statusCode(200);
	}

	@Test
	public void whenRequestGetUserIdPosts_thenOk(){
		given().auth().preemptive()
				.basic(USERNAME, PASS)
				.when().get("/api/users/{id}/posts", 1)
				.then().statusCode(200);
	}

	@Test
	public void whenRequestGetPosts_thenOk(){
		given().auth().preemptive()
				.basic("user2", "password")
				.when().get("/api/posts")
				.then().statusCode(200);
	}

	@Test
	public void whenRequestPostPosts_thenCreated(){
		User newUser = new User();
		newUser.setId((long)1);
		Post newPost = new Post("New Title","New Post to api", newUser);
		given().auth().preemptive()
				.basic(USERNAME, PASS)
				.contentType("application/json").body(newPost)
				.when().post("/api/posts")
				.then().statusCode(201);
	}

	@Test
	public void whenRequestGetPostId_thenOk(){
		given().auth().preemptive()
				.basic(USERNAME, PASS)
				.when().get("/api/posts/{id}", 5)
				.then().statusCode(200);
	}

	@Test
	public void whenRequestPutExistingPost_thenOk(){
		User newUser = new User();
		newUser.setId((long)1);
		Post newPost = new Post("New Title","New Post to api", newUser);
		given().auth().preemptive()
				.basic(USERNAME, PASS)
				.contentType("application/json").body(newPost)
				.when().put("/api/posts/{id}", 1)
				.then().statusCode(200);
	}

	@Test
	public void whenRequestPutNewPost_thenCreated(){
		User newUser = new User();
		newUser.setId((long)1);
		Post newPost = new Post("New Title","New Post to api", newUser);
		given().auth().preemptive()
				.basic(USERNAME, PASS)
				.contentType("application/json").body(newPost)
				.when().put("/api/posts/{id}", 10)
				.then().statusCode(201);
	}

	@Test
	public void whenRequestDeletePostId_thenOk(){
		given().auth().preemptive()
				.basic(USERNAME, PASS)
				.when().delete("/api/posts/{id}", 1)
				.then().statusCode(200);
	}
}
